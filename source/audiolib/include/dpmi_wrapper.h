/*
Copyright (C) 1994-1995 Apogee Software, Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/**********************************************************************
   module: DPMI.H

   author: James R. Dose
   date:   March 31, 1994

   Inline functions for performing DPMI calls.

   (c) Copyright 1994 James R. Dose.  All Rights Reserved.
**********************************************************************/

/* Reduced to a light wrapper around bdpmi */

#ifndef DPMI_WRAPPER_H
#define DPMI_WRAPPER_H

#include "bdpmi.h"
#include "compat.h"

enum DPMI_Errors
   {
   DPMI_Warning = -2,
   DPMI_Error   = -1,
   DPMI_Ok      = 0
   };

static FORCE_INLINE int32_t DPMI_LockMemory( volatile void *address, unsigned length )
{
    dpmi_lock_mem(address, length); return DPMI_Ok;
}

static FORCE_INLINE int32_t DPMI_LockMemoryRegion( volatile void *start, volatile void *end )
{
    dpmi_lock_mem(start, ( volatile char * )end - ( volatile char * )start); return DPMI_Ok;
}

static FORCE_INLINE int32_t DPMI_UnlockMemory( volatile void *address, unsigned length )
{
    dpmi_unlock_mem(address, length); return DPMI_Ok;
}

static FORCE_INLINE int32_t DPMI_UnlockMemoryRegion( volatile void *start, volatile void *end )
{
    dpmi_unlock_mem(start, ( volatile char * )end - ( volatile char * )start); return DPMI_Ok;
}

#define DPMI_Lock( variable ) \
   ( DPMI_LockMemory( &( variable ), sizeof( variable ) ) )

#define DPMI_Unlock( variable ) \
   ( DPMI_UnlockMemory( &( variable ), sizeof( variable ) ) )

#endif
