/*
Copyright (C) 1994-1995 Apogee Software, Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/**********************************************************************
   module: BLASTER.H

   author: James R. Dose
   date:   February 4, 1994

   Public header for BLASTER.C

   (c) Copyright 1994 James R. Dose.  All Rights Reserved.
**********************************************************************/

#ifndef DRIVER_BLASTER_H
#define DRIVER_BLASTER_H

#include "compat.h"

typedef struct
   {
   int32_t Address;
   int32_t Type;
   int32_t Interrupt;
   int32_t Dma8;
   int32_t Dma16;
   int32_t Midi;
   int32_t Emu;
   } BLASTER_CONFIG;

extern BLASTER_CONFIG BLASTER_Config;
extern int32_t BLASTER_DMAChannel;

#define UNDEFINED -1

enum BLASTER_ERRORS
   {
   BLASTER_Warning = -2,
   BLASTER_Error = -1,
   BLASTER_Ok = 0,
   BLASTER_EnvNotFound,
   BLASTER_AddrNotSet,
   BLASTER_DMANotSet,
   BLASTER_DMA16NotSet,
   BLASTER_InvalidParameter,
   BLASTER_CardNotReady,
   BLASTER_NoSoundPlaying,
   BLASTER_InvalidIrq,
   BLASTER_UnableToSetIrq,
   BLASTER_DmaError,
   BLASTER_NoMixer,
   BLASTER_DPMI_Error,
   BLASTER_OutOfMemory
   };

enum BLASTER_Types
   {
   SB     = 1,
   SBPro  = 2,
   SB20   = 3,
   SBPro2 = 4,
   SB16   = 6
   };

#define BLASTER_MinCardType    SB
#define BLASTER_MaxCardType    SB16

#define STEREO      1
#define SIXTEEN_BIT 2

#define MONO_8BIT    0
#define STEREO_8BIT  ( STEREO )
#define MONO_16BIT   ( SIXTEEN_BIT )
#define STEREO_16BIT ( STEREO | SIXTEEN_BIT )

#define BLASTER_MaxMixMode        STEREO_16BIT

#define MONO_8BIT_SAMPLE_SIZE    1
#define MONO_16BIT_SAMPLE_SIZE   2
#define STEREO_8BIT_SAMPLE_SIZE  ( 2 * MONO_8BIT_SAMPLE_SIZE )
#define STEREO_16BIT_SAMPLE_SIZE ( 2 * MONO_16BIT_SAMPLE_SIZE )

#define BLASTER_DefaultSampleRate 11000
#define BLASTER_DefaultMixMode    MONO_8BIT
#define BLASTER_MaxIrq            15

int32_t BLASTER_GetError(void);
const char *BLASTER_ErrorString( int32_t ErrorNumber );
void  BLASTER_EnableInterrupt( void );
void  BLASTER_DisableInterrupt( void );
int32_t   BLASTER_WriteDSP( uint32_t data );
int32_t   BLASTER_ReadDSP( void );
int32_t   BLASTER_ResetDSP( void );
int32_t   BLASTER_GetDSPVersion( void );
void  BLASTER_SpeakerOn( void );
void  BLASTER_SpeakerOff( void );
void  BLASTER_SetPlaybackRate( uint32_t rate );
uint32_t BLASTER_GetPlaybackRate( void );
int32_t   BLASTER_SetMixMode( int32_t mode );
void  BLASTER_StopPlayback( void );
int32_t   BLASTER_SetupDMABuffer( char *BufferPtr, int32_t BufferSize, int32_t mode );
int32_t   BLASTER_GetCurrentPos( void );
int32_t   BLASTER_DSP1xx_BeginPlayback( int32_t length );
int32_t   BLASTER_DSP2xx_BeginPlayback( int32_t length );
int32_t   BLASTER_DSP4xx_BeginPlayback( int32_t length );
int32_t   BLASTER_BeginBufferedRecord( char *BufferStart, int32_t BufferSize,
          int32_t NumDivisions, uint32_t SampleRate, int32_t MixMode,
          void ( *CallBackFunc )( void ) );
int32_t   BLASTER_BeginBufferedPlayback( char *BufferStart,
         int32_t BufferSize, int32_t NumDivisions, uint32_t SampleRate,
         int32_t MixMode, void ( *CallBackFunc )( void ) );
void  BLASTER_WriteMixer( int32_t reg, int32_t data );
int32_t   BLASTER_ReadMixer( int32_t reg );
int32_t   BLASTER_GetVoiceVolume( void );
int32_t   BLASTER_SetVoiceVolume( int32_t volume );
int32_t   BLASTER_GetMidiVolume( void );
int32_t   BLASTER_SetMidiVolume( int32_t volume );
int32_t   BLASTER_CardHasMixer( void );
void  BLASTER_SaveVoiceVolume( void );
void  BLASTER_RestoreVoiceVolume( void );
void  BLASTER_SaveMidiVolume( void );
void  BLASTER_RestoreMidiVolume( void );
int32_t   BLASTER_GetEnv( BLASTER_CONFIG *Config );
int32_t   BLASTER_SetCardSettings( BLASTER_CONFIG Config );
int32_t   BLASTER_GetCardSettings( BLASTER_CONFIG *Config );
int32_t   BLASTER_GetCardInfo( int32_t *MaxSampleBits, int32_t *MaxChannels );
void  BLASTER_SetCallBack( void ( *func )( void ) );
void  BLASTER_SetupWaveBlaster( void );
void  BLASTER_ShutdownWaveBlaster( void );
int32_t   BLASTER_Init( void );
void  BLASTER_Shutdown( void );
void  BLASTER_UnlockMemory( void );
int32_t   BLASTER_LockMemory( void );

int32_t BLASTER_PCM_Init(int32_t *mixrate, int32_t *numchannels, void * initdata);
void BLASTER_PCM_Shutdown(void);
int32_t BLASTER_PCM_BeginPlayback(char *BufferStart, int32_t BufferSize,
                                  int32_t NumDivisions, void ( *CallBackFunc )( void ) );
void BLASTER_PCM_StopPlayback(void);
void BLASTER_PCM_Lock(void);
void BLASTER_PCM_Unlock(void);

#endif
