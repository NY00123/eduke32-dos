/*
Copyright (C) 1994-1995 Apogee Software, Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/**********************************************************************
   Digitized sound playback via the PC Speaker, using PWM.
   Partially based on code by James R. Rose.

   (c) Copyright 1994 James R. Dose.  All Rights Reserved.
**********************************************************************/

#ifndef PCDIGI_H
#define PCDIGI_H

enum PCDigi_ERRORS
   {
   PCDigi_Warning = -2,
   PCDigi_Error   = -1,
   PCDigi_Ok      = 0,
   PCDigi_DPMI_Error
   };

int32_t PCDigi_GetError(void);
const char *PCDigi_ErrorString( int32_t ErrorNumber );

int32_t PCDigi_PCM_Init(int32_t *mixrate, int32_t *numchannels, void * initdata);
void PCDigi_PCM_Shutdown(void);
int32_t PCDigi_PCM_BeginPlayback(char *BufferStart, int32_t BufferSize,
                                 int32_t NumDivisions, void ( *CallBackFunc )( void ) );
void PCDigi_PCM_StopPlayback(void);
void PCDigi_PCM_Lock(void);
void PCDigi_PCM_Unlock(void);

#endif
