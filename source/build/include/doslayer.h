// DOS interface layer for the Build Engine

#ifndef build_interface_layer_
#define build_interface_layer_ DOS

#include "compat.h"
#include <dpmi.h>

extern uint32_t maxrefreshfreq;

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    _go32_dpmi_seginfo old_info, new_info;
} dos_vec_data_t;

void dos_get_old_vect(dos_vec_data_t *data, uint32_t num);
void dos_set_new_vect(dos_vec_data_t *data, uint32_t num, void (*handler)(void));
void dos_restore_vect(dos_vec_data_t *data, uint32_t num);

void dos_settextmode(void);

#ifdef __cplusplus
}
#endif

#endif // build_interface_layer_
