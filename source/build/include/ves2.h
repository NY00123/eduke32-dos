// "Build Engine & Tools" Copyright (c) 1993-1997 Ken Silverman
// Ken Silverman's official web site: "http://www.advsys.net/ken"
// See the included license file "BUILDLIC.TXT" for license info.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <dos.h>
#include <dpmi.h>
#include <sys/nearptr.h>
#include <sys/segments.h>

#define koutp outportb
#define kinp inportb

#pragma pack(push,1)

typedef struct
{
	char VESASignature[4];
	int16_t VESAVersion;
	int32_t OemStringPtr, Capabilities, VideoModePtr;
	int16_t TotalMemory, OemSoftwareRev;
	int32_t OemVendorNamePtr, OemProductNamePtr, OemProductRevPtr;
	char reserved[222], OemDATA[256];
} VBE_vgaInfo;

typedef struct
{
	int16_t ModeAttributes;
	char WinAAtributes, WinBAttributes;
	int16_t WinGranularity, WinSize, WinASegment, WinBSegment;
	int32_t WinFuncPtr;
	int16_t BytesPerScanLine, XResolution, YResolution;
	char XCharSize, YCharSize, NumberOfPlanes, BitsPerPixel;
	char NumberOfBanks, MemoryModel, BankSize, NumberOfImagePages;
	char res1;
	char RedMaskSize, RedFieldPosition;
	char GreenMaskSize, GreenFieldPosition;
	char BlueMaskSize, BlueFieldPosition;
	char RsvdMaskSize, RsvdFieldPosition, DirectColorModeInfo;
	int32_t PhysBasePtr, OffScreenMemOffset;
	int16_t OffScreenMemSize;
	char res2[206];
} VBE_modeInfo;

struct _RMWORDREGS { uint16_t ax, bx, cx, dx, si, di, cflag; };
struct _RMBYTEREGS { uint8_t al, ah, bl, bh, cl, ch, dl, dh; };
typedef union { struct _RMWORDREGS x; struct _RMBYTEREGS h; } RMREGS;
typedef struct { uint16_t es, cs, ss, ds; } RMSREGS;

typedef struct
{
	int32_t edi, esi, ebp, reserved, ebx, edx, ecx, eax;
	int16_t flags, es, ds, fs, gs, ip, cs, sp, ss;
} _RMREGS;

#pragma pack(pop)

int32_t VESABuf_sel = 0, VESABuf_rseg;
int16_t modelist[256];
static char modeschecked = 0;
static int32_t isvesamode;
int32_t imageSize, maxpages;
int32_t buffermode, origbuffermode, linearmode;
int32_t setactiveentry = 0, setvisualentry = 0, setpaletteentry = 0;
char permanentupdate = 0, vgacompatible;
int16_t visualpagelookup[64][2];
int32_t activepagelookup[64];
static int32_t davesapageshift;
static VBE_vgaInfo vgaInfo;
int32_t globlinplace;
static int32_t backlinaddress = 0;  //Save address for free operation (0x801)

#define backupsegs()
#define restoresegs()
#if 0
int16_t ods, oes, oss;
static FORCE_INLINE void backupsegs(void)
{
	__asm__ __volatile__("mov %%ds, %0\n\tmov %%es, %1\n\tmov %%ss, %2":"=r"(ods),"=r"(oes),"=r"(oss)::"eax");
}

static FORCE_INLINE void restoresegs(void)
{
	__asm__ __volatile__("mov %0, %%ds\n\tmov %1, %%es\n\tmov %2, %%ss"::"r"(ods),"r"(oes),"r"(oss):"eax");
}
#endif

#if 0
#pragma aux backupsegs =\
	"mov ods, ds",\
	"mov oes, es",\
	"mov oss, ss",\
	modify [eax]\

#pragma aux restoresegs =\
	"mov ds, ods",\
	"mov es, oes",\
	"mov ss, oss",\
	modify [eax]\

#endif

void setvmode(int mode)
{
  __asm__ __volatile__("mov %0, %%eax\n\tint $0x10"::"r"(mode):"eax");
}

static FORCE_INLINE int32_t vesasetpalette(int32_t a, int32_t b, int32_t c, int32_t d, int32_t esi, char *edi)
{
	__asm__ __volatile__("call _setpaletteentry"
	                     :"=a"(a):"a"(a),"b"(b),"c"(c),"d"(d),"S"(esi),"D"(edi):"memory");
	return a;
}

void DPMI_int86(int32_t intno, RMREGS *in, RMREGS *out)
{
	__dpmi_regs regs;
	Bmemset(&regs, 0, sizeof(regs));
	regs.x.di = in->x.di;
	regs.x.si = in->x.si;
	regs.x.bx = in->x.bx;
	regs.x.dx = in->x.dx;
	regs.x.cx = in->x.cx;
	regs.x.ax = in->x.ax;
	__dpmi_int(intno, &regs);
	out->x.di = regs.x.di;
	out->x.si = regs.x.si;
	out->x.bx = regs.x.bx;
	out->x.dx = regs.x.dx;
	out->x.cx = regs.x.cx;
	out->x.ax = regs.x.ax;
	out->x.cflag = regs.x.flags;
}

void DPMI_int86x(int32_t intno, RMREGS *in, RMREGS *out, RMSREGS *sregs)
{
	__dpmi_regs regs;
	Bmemset(&regs, 0, sizeof(regs));
	regs.x.di = in->x.di;
	regs.x.si = in->x.si;
	regs.x.es = sregs->es;
	regs.x.cs = sregs->cs;
	regs.x.ss = sregs->ss;
	regs.x.ds = sregs->ds;
	regs.x.bx = in->x.bx;
	regs.x.dx = in->x.dx;
	regs.x.cx = in->x.cx;
	regs.x.ax = in->x.ax;
	__dpmi_int(intno, &regs);
	out->x.di = regs.x.di;
	out->x.si = regs.x.si;
	sregs->es = regs.x.es;
	sregs->cs = regs.x.cs;
	sregs->ss = regs.x.ss;
	sregs->ds = regs.x.ds;
	out->x.bx = regs.x.bx;
	out->x.dx = regs.x.dx;
	out->x.cx = regs.x.cx;
	out->x.ax = regs.x.ax;
	out->x.cflag = regs.x.flags;
}

static void ExitVBEBuf(void)
{
	union REGS r;
	r.w.ax = 0x101; r.w.dx = VESABuf_sel; //DPMI free real seg
	backupsegs(); int386(0x31,&r,&r); restoresegs();
}

void VBE_callESDI(RMREGS *regs, void *buffer, int32_t size)
{
	RMSREGS sregs;
	union REGS r;

	if (!VESABuf_sel)  //Init Real mode buffer
	{
		r.w.ax = 0x100; r.w.bx = 1024>>4;
		backupsegs(); int386(0x31,&r,&r); restoresegs();
		if (r.w.cflag) { printf("DPMI_allocRealSeg failed!\n"); exit(0); }
		VESABuf_sel = r.w.dx;
		VESABuf_rseg = r.w.ax;
		atexit(ExitVBEBuf);
	}
	sregs.es = VESABuf_rseg;
	regs->x.di = 0;
	dosmemput(buffer,size,16*VESABuf_rseg);
	DPMI_int86x(0x10,regs,regs,&sregs);
	dosmemget(16*VESABuf_rseg,size,buffer);
}

void GetPtrToLFB(int32_t physAddr)
{
	#define LIMIT ((4096*1024)-1)
	int32_t sel;
	union REGS r;

	r.w.ax = 0; r.w.cx = 1;
	backupsegs(); int386(0x31,&r,&r); restoresegs();
	if (r.x.cflag) { printf("DPMI_allocSelector() failed!\n"); exit(0); }
	sel = r.w.ax;
	r.w.ax = 9; r.w.bx = sel; r.w.cx = 0x8092;
	backupsegs(); int386(0x31,&r,&r); restoresegs();

	r.w.ax = 0x800; r.w.bx = physAddr >> 16; r.w.cx = physAddr & 0xffff;
	r.w.si = LIMIT>>16; r.w.di = LIMIT&0xffff;
	backupsegs(); int386(0x31,&r,&r); restoresegs();
	if (r.x.cflag) { printf("DPMI_mapPhysicalToLinear() failed!\n"); exit(0); }
	backlinaddress = globlinplace = ((int32_t)r.w.bx<<16)+r.w.cx;

	r.w.ax = 7; r.w.bx = sel;
	r.w.cx = globlinplace>>16; r.w.dx = globlinplace&0xffff;
	backupsegs(); int386(0x31,&r,&r); restoresegs();
	if (r.x.cflag) { printf("DPMI_setSelectorBase() failed!\n"); exit(0); }

	r.w.ax = 8; r.w.bx = sel;
	r.w.cx = LIMIT>>16; r.w.dx = LIMIT&0xffff;
	backupsegs(); int386(0x31,&r,&r); restoresegs();
	if (r.x.cflag) { printf("DPMI_setSelectorLimit() failed!\n"); exit(0); }
}

void videoGetModes(void)
{
	int32_t i, j, k;
	int16_t *p, *p2;
	VBE_modeInfo modeInfo;
	RMREGS regs;

	if (modeschecked) return;
	modeschecked = 1;

	// HACK for VGA mode 13h
	validmode[0].xdim = 320; validmode[0].ydim = 200;
	validmode[0].fs = 1; // Don't show as windowed in menu
	validmodecnt = 1;
	modelist[0] = -1;

	strncpy(vgaInfo.VESASignature,"VBE2",4);
	regs.x.ax = 0x4f00;
	VBE_callESDI(&regs,&vgaInfo,sizeof(VBE_vgaInfo));
	if ((regs.x.ax != 0x004f) || (strncmp(vgaInfo.VESASignature,"VESA",4)))
		return;

	//if (vgaInfo.VESAVersion < 0x200) return;

		//LfbMapRealPointer
	p = (int16_t *)(((vgaInfo.VideoModePtr&0xffff0000)>>12)+((vgaInfo.VideoModePtr)&0xffff)+__djgpp_conventional_base);
	p2 = modelist;
	while (*p != -1) *p2++ = *p++;
	*p2 = -1;

	for(p=modelist;*p!=-1;p++)
	{
		regs.x.ax = 0x4f01; regs.x.cx = *p;
		VBE_callESDI(&regs,&modeInfo,sizeof(VBE_modeInfo));
		if (regs.x.ax != 0x004f) continue;
		// HACK for VGA mode 13h
		if ((modeInfo.XResolution == 320) && (modeInfo.YResolution == 200)) continue;
		if (!(modeInfo.ModeAttributes&1)) continue; //1 is vbeMdAvailable
		if (modeInfo.MemoryModel != 4) continue;    //4 is vbeMemPK
		if (modeInfo.BitsPerPixel != 8) continue;
		if (modeInfo.NumberOfPlanes != 1) continue;
		// Partial workaround for usage of mouse in menu
		if (!VID_CHECKMODE(modeInfo.XResolution, modeInfo.YResolution)) continue;

		validmode[validmodecnt].extra = *p;
		validmode[validmodecnt].xdim = modeInfo.XResolution;
		validmode[validmodecnt].ydim = modeInfo.YResolution;
		validmode[validmodecnt].fs = 1; // Don't show as windowed in menu
		validmodecnt++;
	}

	for(i=1;i<validmodecnt;i++)
		for(j=0;j<i;j++)
			if (validmode[i].ydim < validmode[j].ydim)
			{
				k = validmode[i].extra; validmode[i].extra = validmode[j].extra; validmode[j].extra = k;
				k = validmode[i].xdim; validmode[i].xdim = validmode[j].xdim; validmode[j].xdim = k;
				k = validmode[i].ydim; validmode[i].ydim = validmode[j].ydim; validmode[j].ydim = k;
			}

	for(i=1;i<validmodecnt;i++)
		for(j=0;j<i;j++)
			if (validmode[i].xdim < validmode[j].xdim)
			{
				k = validmode[i].extra; validmode[i].extra = validmode[j].extra; validmode[j].extra = k;
				k = validmode[i].xdim; validmode[i].xdim = validmode[j].xdim; validmode[j].xdim = k;
				k = validmode[i].ydim; validmode[i].ydim = validmode[j].ydim; validmode[j].ydim = k;
			}
}

int32_t setvesa(int32_t x, int32_t y)
{
	ldiv_t/*div_t*/ dived; // Using ldiv_t since int32_t is long in DJGPP
	int32_t i, j, k;
	int16_t *p1;
	VBE_modeInfo modeInfo;
	RMREGS regs;
	RMSREGS sregs;

	xres = x; yres = y;
	for(k=0;k<validmodecnt;k++)
	{
		regs.x.ax = 0x4f01; regs.x.cx = validmode[k].extra;
		VBE_callESDI(&regs,&modeInfo,sizeof(VBE_modeInfo));
		if (regs.x.ax != 0x004f) continue;
		if (!(modeInfo.ModeAttributes&1)) continue; //1 is vbeMdAvailable

		if (modeInfo.MemoryModel != 4) continue;    //4 is vbeMemPK
		if (modeInfo.BitsPerPixel != 8) continue;
		if (modeInfo.NumberOfPlanes != 1) continue;
		if (modeInfo.XResolution != x) continue;
		if (modeInfo.YResolution != y) continue;

		bytesperline = modeInfo.BytesPerScanLine;
		maxpages = min(modeInfo.NumberOfImagePages+1,64);

		regs.x.ax = 0x4f02;
		regs.x.bx = validmode[k].extra | ((modeInfo.ModeAttributes&128)<<7);
		DPMI_int86(0x10,&regs,&regs);

		if (modeInfo.ModeAttributes&32) vgacompatible = 0;
											else vgacompatible = 1;

		regs.x.ax = 0x4f0a; regs.x.bx = 0;
		DPMI_int86x(0x10,&regs,&regs,&sregs);
		if (regs.x.ax == 0x004f)   //cx is length of protected mode code
		{
			i = (((int32_t)sregs.es)<<4)+((int32_t)regs.x.di);
			p1 = (int16_t *)i;
			setactiveentry = ((int32_t)p1[0]) + i;
			setvisualentry = ((int32_t)p1[1]) + i;
			setpaletteentry = ((int32_t)p1[2]) + i;
				//p1[2] is useless palette function - see vesprot.asm for code
		}

			//Linear mode
		if (modeInfo.ModeAttributes&128) //128 is vbeMdLinear
		{
			GetPtrToLFB(modeInfo.PhysBasePtr);

			linearmode = 1;
			buffermode = (maxpages<=1);
			imageSize = bytesperline*yres;
			j = 0;
			for(i=0;i<maxpages;i++)
			{
				activepagelookup[i] = globlinplace+j;
				j += imageSize;
			}
		}
		else  //Banked mode
		{
				//Get granularity
			switch(modeInfo.WinGranularity)
			{
				case 64: davesapageshift = 0; break;
				case 32: davesapageshift = 1; break;
				case 16: davesapageshift = 2; break;
				case 8: davesapageshift = 3; break;
				case 4: davesapageshift = 4; break;
				case 2: davesapageshift = 5; break;
				case 1: davesapageshift = 6; break;
			}

			linearmode = 0;
			if ((x == 320) && (y == 200) && (maxpages >= 2))
			{
				buffermode = 0;
				imageSize = 65536;
			}
			else
			{
				buffermode = 1;
				imageSize = bytesperline*yres;
			}
		}

		origbuffermode = buffermode;

		j = 0;
		for(i=0;i<maxpages;i++)
		{
			dived = div(j,bytesperline);
			visualpagelookup[i][0] = (int16_t)dived.rem;
			visualpagelookup[i][1] = (int16_t)dived.quot;
			j += imageSize;
		}
		return(0);
	}
	return(-1);
}

void uninitvesa(void)
{
	if (backlinaddress)
	{
		union REGS r;
		r.w.ax = 0x801;
		r.w.bx = (backlinaddress >> 16);
		r.w.cx = (backlinaddress & 0xffff);
		backupsegs(); int386(0x31,&r,&r); restoresegs();
		if (r.x.cflag) { printf("Free Physical Address failed!\n"); }
		backlinaddress = 0;
	}
	VESABuf_sel = 0;
	modeschecked = 0;
}

//
// setpalette() -- set palette values
//
int32_t videoUpdatePalette(int32_t start, int32_t num)
{
	RMREGS regs;
	int32_t i, j;
	char convpal[1024];
	char *dapal = convpal;

	Bassert((start >= 0) && (num >= 0) && (start + num <= (int32_t)sizeof(convpal) / 4));
	for(i=0,j=0;i<num;i++,j+=4)
	{
		convpal[j] = curpalettefaded[i].b>>2;
		convpal[j+1] = curpalettefaded[i].g>>2;
		convpal[j+2] = curpalettefaded[i].r>>2;
	}
	dapal = convpal;

	if ((vgacompatible) || (vgaInfo.VESAVersion < 0x200) || !isvesamode)
	{
		koutp(0x3c8,start);
		for(i=(num>>1);i>0;i--)
		{
			koutp(0x3c9,dapal[2]);
			while (kinp(0x3da)&1)
				;
			while (!(kinp(0x3da)&1))
				;
			koutp(0x3c9,dapal[1]); koutp(0x3c9,dapal[0]);
			koutp(0x3c9,dapal[6]); koutp(0x3c9,dapal[5]); koutp(0x3c9,dapal[4]);
			dapal += 8;
		}
		if (num&1)
		{
			koutp(0x3c9,dapal[2]);
			while (kinp(0x3da)&1)
				;
			while (!(kinp(0x3da)&1))
				;
			koutp(0x3c9,dapal[1]); koutp(0x3c9,dapal[0]);
		}
		return(1);
	}

	if (setpaletteentry)
	{
		i = (vesasetpalette(0x4f09,(vgaInfo.Capabilities&4)<<5,
								  num,start,0L,dapal)&65535);
	}
	else
	{
		regs.x.ax = 0x4f09; regs.h.bl = ((vgaInfo.Capabilities&4)<<5);
		regs.x.cx = num; regs.x.dx = start;
		VBE_callESDI(&regs,dapal,sizeof(dapal)*num);
		i = regs.x.ax;
	}
	if (i != 0x004f) return(0);
	return(1);
}
