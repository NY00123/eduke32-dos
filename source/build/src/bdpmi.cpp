#include <dpmi.h>
#include <sys/nearptr.h> // __djgpp_base_address

extern "C" void check_for_error(int code, const char *str);

void dpmi_lock_mem(volatile void *ptr, long size)
{
    __dpmi_meminfo info;
    info.address = __djgpp_base_address + (long)ptr;
    info.size = size;
    check_for_error(__dpmi_lock_linear_region(&info), "__dpmi_lock_linear_region");
}

void dpmi_unlock_mem(volatile void *ptr, long size)
{
    __dpmi_meminfo info;
    info.address = __djgpp_base_address + (long)ptr;
    info.size = size;
    check_for_error(__dpmi_unlock_linear_region(&info), "__dpmi_unlock_linear_region");
}
