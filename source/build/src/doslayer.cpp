// "Build Engine & Tools" Copyright (c) 1993-1997 Ken Silverman
// Ken Silverman's official web site: "http://www.advsys.net/ken"
// See the included license file "BUILDLIC.TXT" for license info.
// This file has been modified from Ken Silverman's original release

// DOS interface layer for the Build Engine

#include "baselayer.h"
#include "build.h"
#include "compat.h"
#include "doslayer.h"
#include "engine_priv.h"
#include "scancodes.h"
#include "timer.h"
#include "bdpmi.h"
#include "ves2.h"
#include <dpmi.h>
#include <sys/nearptr.h>
#include <dos.h> // disable/enable
#include <sys/segments.h> // _my_cs
#include <pc.h> // inportb/outportb
#include <crt0.h> // _crt0_startup_flags

#define DOS_MOUSE_XDIM 640
#define DOS_MOUSE_YDIM 200

int _crt0_startup_flags = _CRT0_FLAG_LOCK_MEMORY;

/* Mouse input handlers - ported from PRAGMAS.H for most */

static bool mouseisavailable, mousewheelisenabled;

static bool setupmouse(void)
{
    union REGS regs;
    regs.x.ax = 0;
    int86(0x33, &regs, &regs);
    return regs.x.ax;
}

static bool checkformousewheel(void) // Check for CuteMouse wheel support
{
    union REGS regs;
    regs.x.ax = 0x11;
    int86(0x33, &regs, &regs);
    return ((regs.x.ax == 0x574d) && (regs.x.cx & 1));
}

static void readmousexy(int16_t *x, int16_t *y)
{
  union REGS regs;
  regs.x.ax = 11;
  int86(0x33, &regs, &regs);
  *x = regs.x.cx;
  *y = regs.x.dx;
}

static void readmousestatus(int8_t *buttons, int8_t *wheelmotion, int16_t *x, int16_t *y)
{
  union REGS regs;
  regs.x.ax = 3;
  int86(0x33, &regs, &regs);
  *buttons = regs.h.bl;
  *wheelmotion = mousewheelisenabled ? regs.h.bh : 0;
  *x = regs.x.cx;
  *y = regs.x.dx;
}

static void *surface=0;
char quitevent=0, appactive=1;
int32_t xres=-1, yres=-1, bpp=0, fullscreen=0, bytesperline, refreshfreq=-1;
int32_t gfxmode=0;
intptr_t frameplace=0;
int32_t lockcount=0;
char modechange=1;
char offscreenrendering=0;
char videomodereset = 0;
static int32_t vsync_renderlayer;
uint32_t maxrefreshfreq=70;

// fix for mousewheel
int32_t inputchecked = 0;

#ifdef __cplusplus
extern "C"
{
#endif

//
// system_getcvars() -- propagate any cvars that are read post-initialization
//
void system_getcvars(void)
{
}

//
// initprintf() -- prints a formatted string to the intitialization window
//
void initprintf(const char *f, ...)
{
    va_list va;
    char buf[2048];

    va_start(va, f);
    Bvsnprintf(buf, sizeof(buf), f, va);
    va_end(va);

    initputs(buf);
}


//
// initputs() -- prints a string to the intitialization window
//
void initputs(const char *buf)
{
    OSD_Puts(buf);
//    Bprintf("%s", buf);
}

void check_for_error(int code, const char *str)
{
    if (!code)
        return;
    OSD_Printf("%s failed! %d\n", str, code);
    exit(1);
}

void dos_get_old_vect(dos_vec_data_t *data, uint32_t num)
{
    check_for_error(_go32_dpmi_get_protected_mode_interrupt_vector(num, &data->old_info),
                    "_go32_dpmi_get_protected_mode_interrupt_vector");
}

void dos_set_new_vect(dos_vec_data_t *data, uint32_t num, void (*handler)(void))
{
    data->new_info.pm_offset = (unsigned long)handler;
    data->new_info.pm_selector = (unsigned short)_my_cs();
    check_for_error(_go32_dpmi_allocate_iret_wrapper(&data->new_info),
                    "_go32_dpmi_allocate_iret_wrapper");
    check_for_error(_go32_dpmi_set_protected_mode_interrupt_vector(num, &data->new_info),
                    "_go32_dpmi_set_protected_mode_interrupt_vector");
}

void dos_restore_vect(dos_vec_data_t *data, uint32_t num)
{
    check_for_error(_go32_dpmi_set_protected_mode_interrupt_vector(num, &data->old_info),
                    "_go32_dpmi_set_protected_mode_interrupt_vector");
    check_for_error(_go32_dpmi_free_iret_wrapper(&data->new_info),
                    "_go32_dpmi_free_iret_wrapper");
}

void dos_settextmode(void)
{
    if (gfxmode==0)
        return;
    setvmode(0x3);
    gfxmode=0;
}

enum {
    INT_KEY_RELEASED = 0,
    INT_KEY_PRESSED = 1,
    INT_KEY_UNDEF = 2 // Never stored in the "keys" buffer
};

static uint8_t keys[256];
static volatile uint8_t keys_for_int[256];
static int16_t mouse_buttons;

static const uint8_t pause_scancodes[] = {0xe1, 0x1d, 0x45, 0xe1, 0x9d, 0xc5};
static int pause_scancode_index = 0;
static uint8_t extended_scancode = 0;
static volatile uint8_t keymod = 0;
static uint8_t keymod_presses = 0;

enum {
    KMOD_NUM    = 0x20,
    KMOD_CAPS   = 0x40,
};

static void keyboard_handler(void)
{
    uint8_t key = kinp(0x60);
    uint8_t ctrl_kbd = kinp(0x61);
    koutp(0x61,ctrl_kbd|128);
    koutp(0x61,ctrl_kbd&127);
    koutp(0x20,0x20);
    if (key == pause_scancodes[pause_scancode_index])
    {
        if (++pause_scancode_index == ARRAY_SIZE(pause_scancodes))
        {
            pause_scancode_index = 0;
            keys_for_int[0x59] = INT_KEY_PRESSED; // Pause key (press only)
        }
    }
    else if (key == 0xe0)
        extended_scancode = 128;
    else
    {
        uint8_t sc = key & 127;
        int ispressed = (key & 128) == 0;
        if (sc == sc_NumLock)
        {
            if (((keymod_presses & KMOD_NUM) != 0) == ispressed)
                return;
            if (ispressed)
                keymod ^= KMOD_NUM;
            keymod_presses = (keymod_presses & ~KMOD_NUM) | (ispressed ? KMOD_NUM : 0);
        }
        else if (sc == sc_CapsLock)
        {
            if (((keymod_presses & KMOD_CAPS) != 0) == ispressed)
                return;
            if (ispressed)
                keymod ^= KMOD_CAPS;
            keymod_presses = (keymod_presses & ~KMOD_CAPS) | (ispressed ? KMOD_CAPS : 0);
        }

        keys_for_int[sc|extended_scancode] = ispressed;
        extended_scancode = 0;
    }
}

static void keyboard_handler_end(void) {} // For locking mem

static dos_vec_data_t keyboard_handler_vec_data;

static int input_isinit = 0;

/* Originally grabbed from run of a modified EDuke32 build with SDL2 */
static const char *keynames[ARRAY_SIZE(g_keyNameTable)] = {
    0,
    "Escape",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
    "-",
    "=",
    "Backspace",
    "Tab",
    "Q",
    "W",
    "E",
    "R",
    "T",
    "Y",
    "U",
    "I",
    "O",
    "P",
    "[",
    "]",
    "Return",
    "Left Ctrl",
    "A",
    "S",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    ";",
    "'",
    "`",
    "Left Shift",
    "\\",
    "Z",
    "X",
    "C",
    "V",
    "B",
    "N",
    "M",
    ",",
    ".",
    "/",
    "Right Shift",
    "Keypad *",
    "Left Alt",
    "Space",
    "CapsLock",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "Numlock",
    "ScrollLock",
    "Keypad 7",
    "Keypad 8",
    "Keypad 9",
    "Keypad -",
    "Keypad 4",
    "Keypad 5",
    "Keypad 6",
    "Keypad +",
    "Keypad 1",
    "Keypad 2",
    "Keypad 3",
    "Keypad 0",
    "Keypad .",
    "SysReq",
    0,
    0,
    "F11",
    "F12",
    "Pause",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "Keypad Enter",
    "Right Ctrl",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "Keypad /",
    0,
    0,
    "Right Alt",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "Home",
    "Up",
    "PageUp",
    0,
    "Left",
    0,
    "Right",
    0,
    "End",
    "Down",
    "PageDown",
    "Insert",
    "Delete",
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    "Left GUI",
    "Right GUI",
    "Menu"
};

//
// initinput() -- init input system
//
int32_t initinput(void)
{
    if (input_isinit)
        return 0;
    dos_get_old_vect(&keyboard_handler_vec_data, 9);
    Bmemset(const_cast<uint8_t *>(keys_for_int), INT_KEY_UNDEF, sizeof(keys_for_int));
    dpmi_lock_mem(keys_for_int, sizeof(keys_for_int));
    dpmi_lock_mem(const_cast<uint8_t *>(pause_scancodes), sizeof(pause_scancodes));
    dpmi_lock_mem(&pause_scancode_index, sizeof(pause_scancode_index));
    dpmi_lock_mem(&extended_scancode, sizeof(extended_scancode));
    dpmi_lock_mem(&keymod, sizeof(keymod));
    dpmi_lock_mem(&keymod_presses, sizeof(keymod_presses));
    dpmi_lock_mem((void*)keyboard_handler, (uintptr_t)keyboard_handler_end-(uintptr_t)keyboard_handler);
    _disable();
    dos_set_new_vect(&keyboard_handler_vec_data, 9, keyboard_handler);
    keymod = *(uint8_t *)(0x417 + __djgpp_conventional_base); // Works only once
    _enable();

    inputdevices = 1;
    mouseisavailable = setupmouse();
    if (mouseisavailable)
    {
        inputdevices |= 2;
        mousewheelisenabled = checkformousewheel();
    }
    g_mouseGrabbed = 0;

    Bmemset(g_keyNameTable, 0, sizeof(g_keyNameTable));
    for (uint32_t i = 0; i < ARRAY_SIZE(g_keyNameTable); ++i)
        Bstrncpy(&g_keyNameTable[i][0], keynames[i] ? keynames[i] : "", sizeof(g_keyNameTable[i])-1);

    input_isinit = 1;
    return 0;
}

//
// uninitinput() -- uninit input system
//
void uninitinput(void)
{
    if (!input_isinit)
        return;

    mouseUninit();

    dos_restore_vect(&keyboard_handler_vec_data, 9);
    dpmi_unlock_mem((void*)keyboard_handler, (uintptr_t)keyboard_handler_end-(uintptr_t)keyboard_handler);
    dpmi_unlock_mem(keys_for_int, sizeof(keys_for_int));
    dpmi_unlock_mem(const_cast<uint8_t *>(pause_scancodes), sizeof(pause_scancodes));
    dpmi_unlock_mem(&pause_scancode_index, sizeof(pause_scancode_index));
    dpmi_unlock_mem(&extended_scancode, sizeof(extended_scancode));
    dpmi_unlock_mem(&keymod, sizeof(keymod));
    dpmi_unlock_mem(&keymod_presses, sizeof(keymod_presses));
    input_isinit = 0;
}

//
// initsystem() -- init systems
//
int32_t initsystem(void)
{
    atexit(uninitsystem);

    if (__djgpp_nearptr_enable() == 0) // Required for direct video memory access
      exit(-1);

    lockcount = 0;

    return 0;
}

//
// uninitsystem() -- uninit systems
//
void uninitsystem(void)
{
    DO_FREE_AND_NULL(surface);
    uninitinput();
    timerUninit();
    dos_settextmode();
}

int32_t handleevents_peekkeys(void)
{
    int i;
    disable();
    for (i = 0; i < 256; ++i)
        if (keys_for_int[i] == INT_KEY_PRESSED)
            break;
    enable();
    return (i < 256);
}

// Use this to translate Ctrl+A to 1, etc.
static int letter_key_to_index(int key)
{
  switch (key)
  {
  case sc_A: return 1;
  case sc_B: return 2;
  case sc_C: return 3;
  case sc_D: return 4;
  case sc_E: return 5;
  case sc_F: return 6;
  case sc_G: return 7;
  case sc_H: return 8;
  case sc_I: return 9;
  case sc_J: return 10;
  case sc_K: return 11;
  case sc_L: return 12;
  case sc_M: return 13;
  case sc_N: return 14;
  case sc_O: return 15;
  case sc_P: return 16;
  case sc_Q: return 17;
  case sc_R: return 18;
  case sc_S: return 19;
  case sc_T: return 20;
  case sc_U: return 21;
  case sc_V: return 22;
  case sc_W: return 23;
  case sc_X: return 24;
  case sc_Y: return 25;
  case sc_Z: return 26;

  default: return 0;
  }
}

static void handleevents_checkkeys(void)
{
    uint8_t curr_keys[256], mod;
    int i, j, k;
    disable();
    mod = keymod;
    for (i = 0; i < 256; ++i)
    {
        curr_keys[i] = keys_for_int[i];
        keys_for_int[i] = INT_KEY_UNDEF;
    }
    enable();
    for (k = 0; k < 256; ++k)
        if (curr_keys[k] != INT_KEY_UNDEF)
        {
            int keyvalue = letter_key_to_index(k);
            bool ispressed = (curr_keys[k] == INT_KEY_PRESSED);
            keys[k] = curr_keys[k];
            i = k;

            if (!(mod & KMOD_NUM))
                switch (i)
                {
                    case sc_kpad_1:
                    case sc_kpad_2:
                    case sc_kpad_3:
                    case sc_kpad_4:
                    case sc_kpad_5:
                    case sc_kpad_6:
                    case sc_kpad_7:
                    case sc_kpad_8:
                    case sc_kpad_9:
                    case sc_kpad_0:
                    case sc_kpad_Period:
                        i |= 0x80; // Translate to non-kpad key
                        break;
                }

            // XXX: see osd.c, OSD_HandleChar(), there are more...
            if (ispressed && !keyBufferFull() &&
                (i == sc_Return || i == sc_kpad_Enter ||
                 i == sc_Escape ||
                 i == sc_BackSpace ||
                 i == sc_Tab ||
                 (keys[sc_LeftControl] && keyvalue)))
            {
                switch (i)
                {
                    case sc_Return: case sc_kpad_Enter: keyvalue = '\r'; break;
                    case sc_Escape: keyvalue = 27; break;
                    case sc_BackSpace: keyvalue = '\b'; break;
                    case  sc_Tab: keyvalue = '\t'; break;
                }
                if (OSD_HandleChar(keyvalue))
                    keyBufferInsert(keyvalue);
            }
            else if (ispressed &&
                     i != OSD_OSDKey() && !keyBufferFull())
            {
                /*
                Necessary for Duke 3D's method of entering cheats to work without showing IMEs.
                SDL_TEXTINPUT is preferable overall, but with bitmap fonts it has no advantage.
                */
                int keyvalue = (i < 128) ? g_keyAsciiTable[i] : 128;

                if ('a' <= keyvalue && keyvalue <= 'z')
                {
                    if ((keys[sc_LeftShift]|keys[sc_RightShift]) ^ (mod & KMOD_CAPS))
                        keyvalue -= 'a'-'A';
                }
                else if ((keys[sc_LeftShift]|keys[sc_RightShift])
                         && (i < 0x40)) // Don't apply this to the keypad
                {
                    switch (keyvalue)
                    {
                        case '\'': keyvalue = '"'; break;
                        case ',': keyvalue = '<'; break;
                        case '-': keyvalue = '_'; break;
                        case '.': keyvalue = '>'; break;
                        case '/': keyvalue = '?'; break;
                        case '0': keyvalue = ')'; break;
                        case '1': keyvalue = '!'; break;
                        case '2': keyvalue = '@'; break;
                        case '3': keyvalue = '#'; break;
                        case '4': keyvalue = '$'; break;
                        case '5': keyvalue = '%'; break;
                        case '6': keyvalue = '^'; break;
                        case '7': keyvalue = '&'; break;
                        case '8': keyvalue = '*'; break;
                        case '9': keyvalue = '('; break;

                        case ';': keyvalue = ':'; break;

                        case '=': keyvalue = '+'; break;

                        case '[': keyvalue = '{'; break;
                        case '\\': keyvalue = '|'; break;
                        case ']': keyvalue = '}'; break;

                        case '`': keyvalue = '~'; break;
                    }
                }
                switch (i)
                {
                    case sc_kpad_Slash: keyvalue = '/'; break;
                    case sc_Kpad_Star: keyvalue = '*'; break;
                    case sc_kpad_Minus: keyvalue = '-'; break;
                    case sc_kpad_Plus: keyvalue = '+'; break;
                }

                if ((unsigned)keyvalue <= 0x7Fu)
                {
                    if (OSD_HandleChar(keyvalue))
                        keyBufferInsert(keyvalue);
                }
            }
            // hook in the osd
            if ((j = OSD_HandleScanCode(i, ispressed)) <= 0)
            {
                if (j == -1)  // osdkey
                {
                    for (j = 0; j < NUMKEYS; ++j)
                    {
                        if (keyGetState(j))
                        {
                            keySetState(j, 0);
                            if (keypresscallback)
                                keypresscallback(j, 0);
                        }
                    }
                }
            }
            else if (ispressed)
            {
                if (!keyGetState(k))
                {
                    keySetState(k, 1);
                    if (keypresscallback)
                        keypresscallback(k, 1);
                }
            }
            else
            {
# if 1
                // The pause key generates a sequence of scancodes
                // on press only, and not on release.
                if (k == 0x59)  // pause
                    continue;
# endif
                keySetState(k, 0);
                if (keypresscallback)
                    keypresscallback(k, 0);
            }
        }
}

static void handleevents_checkmouse(void)
{
    int16_t x, y;
    int8_t buttons, wheelmotion;
    if (!mouseisavailable)
        return;

    if (inputchecked && g_mouseEnabled)
    {
        if (g_mouseCallback)
        {
            if (g_mouseBits & 16)
                g_mouseCallback(5, 0);
            if (g_mouseBits & 32)
                g_mouseCallback(6, 0);
        }
        g_mouseBits &= ~(16 | 32);
    }
    inputchecked = 0;

    readmousestatus(&buttons, &wheelmotion, &x, &y);
    g_mouseAbs.x = x * xdim / DOS_MOUSE_XDIM;
    g_mouseAbs.y = y * ydim / DOS_MOUSE_YDIM;
    if (g_mouseGrabbed)
    {
      readmousexy(&x, &y);
      g_mousePos.x += x;
      g_mousePos.y += y;
    }
    for (int i = 0; i < 3; ++i)
        if ((mouse_buttons ^ buttons) & (1 << i))
        {
           int mask = 1 << i;
           if (i == 0)
               g_mouseClickState = ((buttons & mask) == 0) ? MOUSE_RELEASED : MOUSE_PRESSED;

           if (buttons & mask)
           {
               g_mouseBits |= mask;
           }
           else
               g_mouseBits &= ~mask;

           if (g_mouseCallback)
               g_mouseCallback(i+1, buttons & mask);
        }
    if (wheelmotion > 0)
    {
        g_mouseBits |= 16;
        if (g_mouseCallback)
            g_mouseCallback(5, 1);
    }
    else if (wheelmotion < 0)
    {
        g_mouseBits |= 32;
        if (g_mouseCallback)
            g_mouseCallback(6, 1);
    }
    mouse_buttons = buttons;
}

int32_t handleevents(void)
{
    int32_t rv = 0;

    handleevents_checkkeys();
    handleevents_checkmouse();
    timerUpdate();

    return rv;
}

const char *joyGetName(int32_t what, int32_t num)
{
    UNREFERENCED_PARAMETER(what);
    UNREFERENCED_PARAMETER(num);
    return NULL;
}

void joyScanDevices()
{
}

//
// setjoydeadzone() -- sets the dead and saturation zones for the joystick
//
void joySetDeadZone(int32_t axis, uint16_t dead, uint16_t satur)
{
    UNREFERENCED_PARAMETER(axis);
    UNREFERENCED_PARAMETER(dead);
    UNREFERENCED_PARAMETER(satur);
}

int main(int argc, char *argv[])
{
    maybe_redirect_outputs();
    return app_main(argc, (char const * const *)argv);
}

//
// initmouse() -- init mouse input
//
void mouseInit(void)
{
    mouseGrabInput(g_mouseEnabled = g_mouseLockedToWindow);  // FIXME - SA
}

//
// uninitmouse() -- uninit mouse input
//
void mouseUninit(void)
{
    mouseGrabInput(0);
    g_mouseEnabled = 0;
}

void mouseGrabInput(bool grab)
{
    g_mouseGrabbed = grab;
    g_mousePos.x = g_mousePos.y = 0;
}

void mouseLockToWindow(char a)
{
    if (!(a & 2))
    {
        mouseGrabInput(a);
        g_mouseLockedToWindow = g_mouseGrabbed;
    }
#if 0 // We don't really want to show a possibly flickering cursor
    int32_t show_param = 2-(osd && osd->flags & OSD_CAPTURE);
  __asm__ __volatile__("mov %0, %%eax\n\tint $0x33"::"r"(show_param):"eax");
#endif
}

//
// begindrawing() -- locks the framebuffer for drawing
//
#ifdef DEBUG_FRAME_LOCKING
uint32_t begindrawing_line[BEGINDRAWING_SIZE];
const char *begindrawing_file[BEGINDRAWING_SIZE];
void begindrawing_real(void)
#else
void videoBeginDrawing(void)
#endif
{
    if (lockcount++ > 0)
        return;		// already locked

    static intptr_t backupFrameplace = 0;

    if (inpreparemirror)
    {
        //POGO: if we are offscreenrendering and we need to render a mirror
        //      or we are rendering a mirror and we start offscreenrendering,
        //      backup our offscreen target so we can restore it later
        //      (but only allow one level deep,
        //       i.e. no viewscreen showing a camera showing a mirror that reflects the same viewscreen and recursing)
        if (offscreenrendering)
        {
            if (!backupFrameplace)
                backupFrameplace = frameplace;
            else if (frameplace != (intptr_t)mirrorBuffer &&
                     frameplace != backupFrameplace)
                return;
        }

        frameplace = (intptr_t)mirrorBuffer;

        if (offscreenrendering)
            return;
    }
    else if (offscreenrendering)
    {
        if (backupFrameplace)
        {
            frameplace = backupFrameplace;
            backupFrameplace = 0;
        }
        return;
    }
    else
    {
        frameplace = (intptr_t)surface;
    }

    if (modechange)
    {
        bytesperline = xdim;
        calc_ylookup(bytesperline, ydim);
        modechange=0;
    }
}

//
// enddrawing() -- unlocks the framebuffer
//
void videoEndDrawing(void)
{
    if (!frameplace) return;
    if (lockcount > 1) { lockcount--; return; }
    if (!offscreenrendering) frameplace = 0;
    lockcount = 0;
}

//
// resetvideomode() -- resets the video system
//
void videoResetMode(void)
{
    modeschecked = 0;
}

//
// setgamma
//
int32_t videoSetGamma(void)
{
    return 0;
}

int32_t videoSetMode(int32_t x, int32_t y, int32_t c, int32_t fs)
{
    if ((fs == fullscreen) && (x == xres) && (y == yres) && (c == bpp) && !videomodereset)
        return 0;
    if (c != 8)
        return -1;
    if (surface)
      Xfree(surface);
    surface=Bmalloc(x*y);
    if (!surface)
    {
        OSD_Printf("videoSetMode: Failed to allocate memory for surface!\n");
        return -1;
    }
    isvesamode = ((x != 320) || (y != 200));
    if (!isvesamode)
    {
        setvmode(0x13);
        globlinplace = 0xa0000; // HACK
    }
    else if (setvesa(x, y) < 0)
    {
        DO_FREE_AND_NULL(surface);
        return -1;
    }
    // Should be e.g., 70Hz for VGA mode 13h, but newer hardware may behave differently
    //refreshfreq = 70; // Varies for VESA modes
    gfxmode=1;
    fullscreen = fs;
    xres = x;
    yres = y;
    bpp = c;
    modechange = 1;
    videomodereset = 0;
    return 0;
}

int32_t videoSetVsync(int32_t newSync)
{
    vsync_renderlayer = newSync;
    return newSync;
}

//
// showframe() -- update the display
//
void videoShowFrame(int32_t w)
{
    UNREFERENCED_PARAMETER(w);
    if (vsync_renderlayer)
    {
        while (kinp(0x3da) & 8)
            ;
        while (!(kinp(0x3da) & 8))
            ;
    }
    memcpy((void *)(globlinplace + __djgpp_conventional_base), surface, xdim*ydim);
}

void wm_setapptitle(const char *name)
{
    UNREFERENCED_PARAMETER(name);
}

int32_t wm_msgbox(const char *name, const char *fmt, ...)
{
    char buf[2048];
    va_list va;

    UNREFERENCED_PARAMETER(name);

    va_start(va,fmt);
    vsnprintf(buf,sizeof(buf),fmt,va);
    va_end(va);

    initprintf("wm_msgbox called. Message: %s: %s",name,buf);
    return 0;
}

int32_t wm_ynbox(const char *name, const char *fmt, ...)
{
    char buf[2048];
    va_list va;

    UNREFERENCED_PARAMETER(name);

    va_start(va,fmt);
    vsnprintf(buf,sizeof(buf),fmt,va);
    va_end(va);

    initprintf("wm_ynbox called, this is bad! Message: %s: %s",name,buf);
    initprintf("Returning false..");
    return 0;
}

#ifdef __cplusplus
}
#endif
