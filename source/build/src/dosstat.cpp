/* Copyright (C) 2012 DJ Delorie, see COPYING.DJ for details */
/*
 * Copyright (c) 1994-1996 Eli Zaretskii <eliz@is.elta.co.il>
 *
 * This software may be used freely so long as this copyright notice is
 * left intact.  There is no warranty on this software.
 *
 */

#include "compat.h"
#include <dir.h>
#include <dpmi.h>
#include <io.h>
#include <libc/dosio.h>

/* libc function */
extern "C" time_t           _file_time_stamp(unsigned int);

/* Using DJGPP's implementations of fstat and _getftime,
   most for figuring out the m_time field. */

/* Get time stamp of a DOS file packed as a 32-bit int.
 * This does what Borland's getftime() does, except it doesn't
 * pollute the application namespace and returns an int instead
 * of struct ftime with packed bit-fields.
 */

int
_getftime(int fhandle, unsigned int *dos_ftime)
{
  __dpmi_regs regs;

  regs.x.ax = 0x5700;
  regs.x.bx = fhandle;
  __dpmi_int(0x21, &regs);

  if (regs.x.flags & 1)
  {
    errno = __doserr_to_errno(regs.x.ax);
    return -1;
  }

  *dos_ftime = ((unsigned int)regs.x.dx << 16) + (unsigned int)regs.x.cx;

  return 0;
}

int Bfstat(int fd, struct Bstat *statbuf)
{
    unsigned int dos_ftime;
    statbuf->st_size = filelength(fd);
    statbuf->st_mode = S_IFREG;
    statbuf->st_mtime = 0;
    if (_getftime(fd, &dos_ftime) == 0)
        statbuf->st_mtime = _file_time_stamp(dos_ftime);
    return 0;
}

#define ALL_FILES   (FA_RDONLY|FA_HIDDEN|FA_SYSTEM|FA_DIREC|FA_ARCH)

int Bstat(const char *pathname, struct Bstat *statbuf)
{
    struct ffblk ff_blk;
    unsigned dos_ftime;
    if (findfirst(pathname, &ff_blk, ALL_FILES))
        return -1;
    dos_ftime = ((unsigned short)ff_blk.ff_fdate << 16) +
                 (unsigned short)ff_blk.ff_ftime;
    statbuf->st_mtime = _file_time_stamp(dos_ftime);
    /* File size. */
    statbuf->st_size = ff_blk.ff_fsize;
    if (ff_blk.ff_attrib & 0x10)
        statbuf->st_mode = S_IFDIR;
    else /* FIXME: Naive assume it isn't a symlink, and treat it as file */
        statbuf->st_mode = S_IFREG;
    return 0;
}

