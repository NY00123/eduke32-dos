
#include "timer.h"

#include "build.h"
#include "compat.h"
#ifdef RENDERTYPEDOS
#include "interrup.h"
#include "task_man.h"
#endif

#include <chrono>

using namespace std;
using namespace chrono;

#ifdef RENDERTYPEDOS
#define TIMER_HANDLER_RATE_HZ 256
static unsigned long long timerlastsample;
static volatile unsigned long long timerhandlersample;
static task *timertask;
#else
EDUKE32_STATIC_ASSERT((steady_clock::period::den/steady_clock::period::num) >= 1000000000);

static time_point<steady_clock> timerlastsample;
#endif
static int timerticspersec;
static void(*usertimercallback)(void) = NULL;

#ifdef RENDERTYPEDOS
static void timer_handler(task *UNUSED(Task)) { ++timerhandlersample; }

int      timerGetRate(void)     { return timerticspersec; }
uint32_t timerGetTicks(void)    { return 1000*timerhandlersample/TIMER_HANDLER_RATE_HZ; }
uint64_t timerGetTicksU64(void) { return timerhandlersample; }
uint64_t timerGetFreqU64(void)  { return TIMER_HANDLER_RATE_HZ; }

// Returns the time since an unspecified starting time in milliseconds.
// (May be not monotonic for certain configurations.)
double timerGetHiTicks(void) { return 1000*timerhandlersample/TIMER_HANDLER_RATE_HZ; }
#else
int      timerGetRate(void)     { return timerticspersec; }
uint32_t timerGetTicks(void)    { return duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count(); }
uint64_t timerGetTicksU64(void) { return steady_clock::now().time_since_epoch().count() * steady_clock::period::num; }
uint64_t timerGetFreqU64(void)  { return steady_clock::period::den; }

// Returns the time since an unspecified starting time in milliseconds.
// (May be not monotonic for certain configurations.)
double timerGetHiTicks(void) { return duration<double, nano>(steady_clock::now().time_since_epoch()).count() / 1000000.0; }
#endif

int timerInit(int const tickspersecond)
{
    timerticspersec = tickspersecond;
#ifdef RENDERTYPEDOS
    timerlastsample = 0;
    timertask = TS_ScheduleTask(timer_handler, TIMER_HANDLER_RATE_HZ, 1, NULL);
    TS_Dispatch();
#else
    timerlastsample = steady_clock::now();
#endif

    usertimercallback = NULL;

    return 0;
}

#ifdef RENDERTYPEDOS
void timerUninit(void)
{
    if (timertask)
        TS_Terminate(timertask);
    timertask = NULL;
}
#endif

ATTRIBUTE((flatten)) void timerUpdate(void)
{
#ifdef RENDERTYPEDOS
    auto time = timerhandlersample;
    auto elapsedTime = time - timerlastsample;
    uint64_t numerator = (elapsedTime * (uint64_t) timerticspersec);
#else
    auto time = steady_clock::now();
    auto elapsedTime = time - timerlastsample;

    uint64_t numerator = (elapsedTime.count() * (uint64_t) timerticspersec * steady_clock::period::num);
#endif
    uint64_t freq = timerGetFreqU64();
    int n = tabledivide64(numerator, freq);
    totalclock.setFraction(tabledivide64((numerator - n*timerGetFreqU64()) * 65536, freq));

    if (n <= 0) return;

    totalclock += n;
#ifdef RENDERTYPEDOS
    timerlastsample += n*(uint64_t)TIMER_HANDLER_RATE_HZ/timerticspersec;
#else
    timerlastsample += n*nanoseconds(1000000000/timerticspersec);
#endif

    if (usertimercallback)
        for (; n > 0; n--) usertimercallback();
}

void(*timerSetCallback(void(*callback)(void)))(void)
{
    void(*oldtimercallback)(void);

    oldtimercallback = usertimercallback;
    usertimercallback = callback;

    return oldtimercallback;
}
