//-------------------------------------------------------------------------
/*
Copyright (C) 2010 EDuke32 developers and contributors

This file is part of EDuke32.

EDuke32 is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
//-------------------------------------------------------------------------

/*
 * A no-op reimplementation of Jim Dose's FX_MAN routines.
 */

#include "music.h"

int32_t MUSIC_ErrorCode = MUSIC_Ok;

const char *MUSIC_ErrorString(int32_t ErrorNumber)
{
    UNREFERENCED_PARAMETER(ErrorNumber);
    return NULL;
}

int32_t MUSIC_Init(int32_t SoundCard, int32_t Address)
{
    UNREFERENCED_PARAMETER(SoundCard);
    UNREFERENCED_PARAMETER(Address);
    return MUSIC_Error;
}

int32_t MUSIC_Shutdown(void)
{
    return MUSIC_Ok;
}

void MUSIC_SetVolume(int32_t volume)
{
    UNREFERENCED_PARAMETER(volume);
}

void MUSIC_Continue(void)
{}

void MUSIC_Pause(void)
{}

int32_t MUSIC_StopSong(void)
{
    return MUSIC_Ok;
}

// Duke3D-specific.  --ryan.
// void MUSIC_PlayMusic(char *_filename)
int32_t MUSIC_PlaySong(char *song, int32_t songsize, int32_t loopflag)
{
    UNREFERENCED_PARAMETER(song);
    UNREFERENCED_PARAMETER(songsize);
    UNREFERENCED_PARAMETER(loopflag);
    return MUSIC_Error;
}

void MUSIC_Update(void)
{}
